<?php
/**
 * @file
 * Contains Drupal\salon_manager_widget\Form\MessagesForm.
 */
namespace Drupal\salon_manager_widget\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class MessagesForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'salon_manager_widget.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'welcome_form';
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('salon_manager_widget.adminsettings');

    $form['widget_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your Access Key will be sent to your buisness email address once you enable the web widgets in your iPad app.
<br>Please go to Side Menu > Admin Dashboard > Settings > Website Widgets.<br><br>Enter Access Key'),
      '#description' => $this->t(''),
      '#attributes' => array(
        'placeholder' => t('Enter Access Key'),
      ),
      '#default_value' => $config->get('widget_id'),
      '#required' => TRUE,
    ];
    $form['widget_key'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Widget Id'),
      '#description' => $this->t(''),
      '#default_value' => $config->get('widget_key'),
      
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function callAPI($method, $url, $data){
   $curl = curl_init();
   switch ($method){
      case "POST":
         curl_setopt($curl, CURLOPT_POST, 1);
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
         break;
      case "PUT":
         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
         break;
      default:
         if ($data)
            $url = sprintf("%s?%s", $url, http_build_query($data));
   }
   // OPTIONS:
   curl_setopt($curl, CURLOPT_URL, $url);
   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
           'Content-Type: application/json',
   ));
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
   // EXECUTE:
   $result = curl_exec($curl);
   if(!$result){die("Connection Failure");}
   curl_close($curl);
   return $result;
}
    
public function validateForm(array &$form, FormStateInterface $form_state) {
    $apikey= $form_state->getValue('widget_id');
    $data_array =  array("accessKey" => $apikey);
    $update_plan = $this->callAPI('PUT', 'https://widgets.api.salonmanager.com/v1/web-access-keys/verify', json_encode($data_array));
    $response = json_decode($update_plan, true);
   // $response['data']['widgetId']='ae0362e0-04d7-11ec-881d-bb9d6629c8b1';
    if(isset($response['data']['widgetId'])==""){
        $form_state->setErrorByName(
        'Access',
        $this->t("Invalid Access Key")
      );
       $this->config('salon_manager_widget.adminsettings')
      ->set('widget_id', '')
      ->save();
    $this->config('salon_manager_widget.adminsettings')
      ->set('widget_key', '')
      ->save();
    }
  }


  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $apikey= $form_state->getValue('widget_id');
    $data_array =  array("accessKey" => $apikey);
    $update_plan = $this->callAPI('PUT', 'https://widgets.api.salonmanager.net/v1/web-access-keys/verify', json_encode($data_array));
    $response = json_decode($update_plan, true);
    //$response['data']['widgetId']='ae0362e0-04d7-11ec-881d-bb9d6629c8b1';
    if(isset($response['data']['widgetId'])!=""){
    $this->config('salon_manager_widget.adminsettings')
      ->set('widget_id', $form_state->getValue('widget_id'))
      ->save();
    $this->config('salon_manager_widget.adminsettings')
      ->set('widget_key', $response['data']['widgetId'])
      ->save();
      drupal_flush_all_caches() ;
  } 
  }
}