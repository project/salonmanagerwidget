<?php

/**
 * @file
 * Provides basic hello world message functionality.
 */

namespace Drupal\salon_manager_widget\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class HelloWorldController.
 *
 * @package Drupal\salon_manager_widget\Controller
 */
class SalonController extends ControllerBase {

  /**
   * Say Hello.
   *
   * @return array
   *   Markup.
   */
  public function salon() {
    return ['#markup' => $this->t("Salon World!")];
  }

}