INTRODUCTION
------------

Salon Manager Widget module for Drupal 8.x.
This module allows you to add style and scripts in different region of the page
from the front-end. You don't need to open any file for this purpose.

This module adds the styles and scripts on all over the site. 

REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. 

CONFIGURATION
-------------

 * Configure Widget access key in Administration

MAINTAINERS
-----------

Current maintainers:
 * Salon Manager
 

This project has been sponsored by:
 * Salon Manager
   
